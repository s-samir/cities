<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'clients' => array(
                    'type'    => 'Literal',
                    'options' => array(
                            'route'    => '/client/',
                            'defaults' => array(
                                    '__NAMESPACE__' => 'Application\Controller',
                                    'controller'    => 'Client',
                                    'action'        => 'index',
                            ),
                    ),
                    'may_terminate' => true,
                    'child_routes' => array(
                            'create' => array(
                                    'type'    => 'Segment',
                                    'options' => array(
                                            'route'    => 'create/',                                      
                                            'defaults' => array(
                                            '__NAMESPACE__' => 'Application\Controller',
                                            'controller'    => 'Client',
                                            'action'        => 'create',
                                            ),
                                    ),
                            ),
                            'getclients'=>array(
                                    'type'    => 'Segment',
                                    'options' => array(
                                            'route'    => 'getclients/',                                      
                                            'defaults' => array(
                                            '__NAMESPACE__' => 'Application\Controller',
                                            'controller'    => 'Client',
                                            'action'        => 'getClients',
                                            ),
                                    ),
                            ),
                            'getclientid'=>array(
                                    'type'    => 'Segment',
                                    'options' => array(
                                            'route'    => 'getclientid/',
                                            'defaults' => array(
                                                    '__NAMESPACE__' => 'Application\Controller',
                                                    'controller'    => 'Client',
                                                    'action'        => 'getClienId',
                                            ),
                                    ),
                            ),
                    )
            ),
            'upload'=>array(
                    'type'    => 'Literal',
                    'options' => array(
                            'route'    => '/upload/',
                            'defaults' => array(
                                    '__NAMESPACE__' => 'Application\Controller',
                                    'controller'    => 'UploadForm',
                                    'action'        => 'index',
                            ),
                    ),
                    'may_terminate' => true,
                    'child_routes' => array(
                            'create' => array(
                                    'type'    => 'Segment',
                                    'options' => array(
                                            'route'    => 'create/',
                                            'defaults' => array(
                                                    '__NAMESPACE__' => 'Application\Controller',
                                                    'controller'    => 'Import',
                                                    'action'        => 'create',
                                            ),
                                    ),
                            ),
            
                    )),
            'import'=>array(
                    'type'    => 'Literal',
                    'options' => array(
                            'route'    => '/import/',
                            'defaults' => array(
                                    '__NAMESPACE__' => 'Application\Controller',
                                    'controller'    => 'Import',
                                    'action'        => 'index',
                            ),
                    ),
            'may_terminate' => true,
            'child_routes' => array(
                    'create' => array(
                            'type'    => 'Segment',
                            'options' => array(
                                    'route'    => 'create/',
                                    'defaults' => array(
                                            '__NAMESPACE__' => 'Application\Controller',
                                            'controller'    => 'Import',
                                            'action'        => 'create',
                                    ),
                            ),
                    ),
                    
            )),
            'export'=>array(
                    'type'    => 'Literal',
                    'options' => array(
                            'route'    => '/export/',
                            'defaults' => array(
                                    '__NAMESPACE__' => 'Application\Controller',
                                    'controller'    => 'export',
                                    'action'        => 'index',
                            ),
                    ),
                    'may_terminate' => true,
                    'child_routes' => array(
                            'default' => array(
                                    'type'    => 'Segment',
                                    'options' => array(
                                            'route'    => 'create/',
                                            'defaults' => array(
                                                    '__NAMESPACE__' => 'Application\Controller',
                                                    'controller'    => 'Export',
                                                    'action'        => 'create',
                                            ),
                                    ),
                            ),
                    )),
            'factures'=>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/facture/',
                    'defaults' => array(
                            '__NAMESPACE__' => 'Application\Controller',
                            'controller'    => 'Facture',
                            'action'        => 'index',
                    ),
            ),
            'may_terminate' => true,
            'child_routes' => array(
                    'default' => array(
                            'type'    => 'Segment',
                            'options' => array(
                                    'route'    => 'create/',
                                    'defaults' => array(
                                            '__NAMESPACE__' => 'Application\Controller',
                                            'controller'    => 'Facture',
                                            'action'        => 'create',
                                    ),
                            ),
                    ),
                    'savefs' => array(
                            'type'    => 'Segment',
                            'options' => array(
                                    'route'    => 'savefraissup/',
                                    'defaults' => array(
                                            '__NAMESPACE__' => 'Application\Controller',
                                            'controller'    => 'Facture',
                                            'action'        => 'savefraissup',
                                    ),
                            ),
                    ),
                    'getop' => array(
                            'type'    => 'Segment',
                            'options' => array(
                                    'route'    => 'getoperation/',
                                    'defaults' => array(
                                            '__NAMESPACE__' => 'Application\Controller',
                                            'controller'    => 'Facture',
                                            'action'        => 'getOperation',
                                    ),
                            ),
                    ),
                    'getclt' => array(
                            'type'    => 'Segment',
                            'options' => array(
                                    'route'    => 'getclientname/',
                                    'defaults' => array(
                                            '__NAMESPACE__' => 'Application\Controller',
                                            'controller'    => 'Facture',
                                            'action'        => 'getClientName',
                                    ),
                            ),
                    ),
            )
            ),
            'reglement'=>array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/reglement/',
                    'defaults' => array(
                            '__NAMESPACE__' => 'Application\Controller',
                            'controller'    => 'Facture',
                            'action'        => 'savereglement',
                    ),
                ),
            ),
            'fournisseurs'=>array(
                    'type'    => 'Literal',
                    'options' => array(
                            'route'    => '/fournisseur/',
                            'defaults' => array(
                                    '__NAMESPACE__' => 'Application\Controller',
                                    'controller'    => 'Fournisseur',
                                    'action'        => 'index',
                            ),
                    ),
                    'may_terminate' => true,
                    'child_routes' => array(
                            'default' => array(
                                    'type'    => 'Segment',
                                    'options' => array(
                                            'route'    => 'create/',
                                            'defaults' => array(
                                                    '__NAMESPACE__' => 'Application\Controller',
                                                    'controller'    => 'Fournisseur',
                                                    'action'        => 'create',
                                            ),
                                    ),
                            ),
                    )
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                )),
            
           )),
    
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Client' => 'Application\Controller\ClientController',
            'Application\Controller\Facture' => 'Application\Controller\FactureController',
            'Application\Controller\Import' => 'Application\Controller\ImportController',
            'Application\Controller\Export' => 'Application\Controller\ExportController',
            'Application\Controller\Fournisseur' => 'Application\Controller\FournisseurController',
            'Application\Controller\UploadForm' => 'Application\Controller\UploadFormController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'application/client/index' => __DIR__ . '/../view/application/client/index.phtml',
            'application/facture/index' => __DIR__ . '/../view/application/facture/index.phtml',
            'application/fournisseur/index' => __DIR__ . '/../view/application/fournisseur/index.phtml',
            'application/import/index' => __DIR__ . '/../view/application/import/index.phtml',
            'application/export/index' => __DIR__ . '/../view/application/export/index.phtml',
            'application/upload/index' => __DIR__ . '/../view/application/uploadForm/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
                'ViewJsonStrategy',
        ),
    ),
    'funcs'=>array(
    	array("label"=>"Accueil", "url"=>"home"),
    	array("label"=>"Clients", "url"=>"clients"),
    	array("label"=>"Fournisseurs", "url"=>"fournisseurs"),
    	array("label"=>"Personnels", "url"=>"clients"),
    	array("label"=>"National", "url"=>"import"),
    	array("label"=>"Import", "url"=>"import"),
    	array("label"=>"Export", "url"=>"export"),
    	array("label"=>"Factures", "url"=>"factures"),
    	array("label"=>"Réglements", "url"=>"clients"),
    	array("label"=>"Documents", "url"=>"clients"),
    ),
    'clientactions'=>array(
            array("label"=>"Modifier", "url"=>"home"),
            array("label"=>"Supprimer", "url"=>"clients"),
            array("label"=>"Rapport", "url"=>"home"),
    ),
    'factureactions'=>array(
            array("label"=>"Modifier", "url"=>"facture"),
            array("label"=>"Supprimer", "url"=>"clients"),
            array("label"=>"Rapport", "url"=>"home"),
    ),
    
    'sectionmenu'=>array(
    	'clients'=>array(
    	array(
    	        'label'=>'Liste des clients',
    	        'url'=>'home',
    	),
    	    array(
    	    	'label'=>'Nouveau Client',
    	    	'url'=>'',
    	    ),
    	    array(
    	        'label'=>'Importer la liste',
    	        'url'=>'home',
    	    ),
    	    array("label"=>"Exporter la liste", 
    	          "url"=>"home"
    	    ),
    	    array("label"=>"Imprimer la liste", 
    	          "url"=>"home"
    	    ),
    	    
    ),
    'fournisseurs'=>array(
            array(
                    'label'=>'Liste des fournisseurs',
                    'url'=>'home',
            ),
            array(
                    'label'=>'Saisir une facture',
                    'url'=>'',
            ),
            array(
                    'label'=>'Faire un achat',
                    'url'=>'home',
            ),
            array("label"=>"Exporter la liste",
                    "url"=>"home"
            ),
            array("label"=>"Imprimer la liste",
                    "url"=>"home"
            ),
            	
    ),
    'facture'=>array(
    array(
            'label'=>'Liste des factures',
            'url'=>'home',
    ),
    array(
            'label'=>'Nouvelle facture',
            'url'=>'',
    ),
    array(
            'label'=>'Importer la liste',
            'url'=>'home',
    ),
    array("label"=>"Exporter la liste",
            "url"=>"home"
    ),
    array("label"=>"Imprimer la liste",
            "url"=>"home"
    ),
    ),
    'import'=>array(
            array(
                    'label'=>'Liste des dossiers Imports',
                    'url'=>'home',
            ),
            array(
                    'label'=>'Nouveau dossier',
                    'url'=>'',
            ),
            array(
                    'label'=>'Importer la liste',
                    'url'=>'home',
            ),
            array("label"=>"Exporter la liste",
                    "url"=>"home"
            ),
            array("label"=>"Imprimer la liste",
                    "url"=>"home"
            ),
    ),
    ),
);
