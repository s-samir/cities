<?php
namespace Models\Tables;

use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\Controller\AbstractActionController;




class etablissementTable extends TableGateway implements injectController{
    
    protected $sm; 
    protected $ctl;
    
    public function __construct($adpter){
        parent::__construct('etablissement', $adpter);
    }
    
    public function setController(AbstractActionController $ctl){
    	$this->ctl = $ctl;
    }
    
	public function getList(){ 
	    $result = array();
		$res = $this->select();		
		return $res->toArray();
	}
	
	public function getArgument(){
		
	}
	
	public function setArgument($args){
		$this->args= $args;
	}
	
	public function add(){
	    $data = $_POST;
	    $ll = $this->getLatlng($data['adresse'], $data['ville']);
	    //var_dump($ll);
	    $datas = array_merge($data, $ll);
	    try{
		    $this->insert($datas);
		    //$this->ctl->redirect()->toUrl('http://localhost/cities/public/admin/addetablissement');
	    }catch (\Exception $e){
	        var_dump("dbbbbbbbbbbbbbb probleme a sidi");
	    	var_dump($e);
	    };
	}
	
    
	/* (non-PHPdoc)
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
     */
    public function setServiceLocator (
            \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $this->sm = $serviceLocator;
        
    }

	/* (non-PHPdoc)
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
     */
    public function getServiceLocator ()
    {
        return $this->sm;
        
    }
    
    public function getLatlng($ad, $vi, $co='Maroc'){
        $add = urldecode($ad.' '.$vi.' '.$co);
        $urlbase = 'https://maps.googleapis.com/maps/api/geocode/json?';
        $adresse = 'address='.$add.'&';
        $adresse = str_replace(' ', '%20', $adresse); 
        $key = 'key=AIzaSyA3y6nuWbQB9VkT0bLPYPl28tLnq-Bj5bk';
        $url = $urlbase.$adresse.$key;
        //var_dump(urldecode($url));
    	$client = new \Zend\Http\Client($url, 
    	        array(
    		          'adapter'=>'\Zend\Http\Client\Adapter\Curl',
    	              'curloptions'=>array(CURLOPT_SSL_VERIFYPEER=>false)
    	));
    	try{
    	    $response = $client->send();
    	}catch(\Exception $e){
    		echo $e->getMessage();
    	}
    	$lar = json_decode($response->getBody(), true);
    	if($lar['status'] == 'OK'){
    	    return $lar['results'][0]['geometry']['location'];
    	}else{
    		return  array('lat'=>'NON_IDENTIFIE', 'lng'=>'NON_IDENTIFIE');
    	}
    }



}