<?php
namespace Models\Tables;

use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\Controller\AbstractActionController;

class categoriesTable extends TableGateway implements injectController{
    
    protected $sm; 
    protected $ctl;
    
    public function __construct($adapter){
    	parent::__construct('categories', $adapter);
    }
    
    public function setController(AbstractActionController $ctl){
        $this->ctl = $ctl;
    }
    	
	public function getList(){ 	    
	    $result = array();
	    try{
		    $result = $this->select(array())->toArray();
	    }catch (\Exception $e){
	    	echo "un probleme";
	    }
		return $result;
	}
	
	public function add(){
	    try{
	        $this->insert($_POST);
	        $this->ctl->redirect()->toUrl('http://localhost/cities/public/admin/addcategorie');
	    }catch (\Exception $e){
	        var_dump("");
	        var_dump($e);
	    };
	}
	
    
	/* (non-PHPdoc)
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
     */
    public function setServiceLocator (
            \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $this->sm = $serviceLocator;
        
    }

	/* (non-PHPdoc)
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
     */
    public function getServiceLocator ()
    {
        return $this->sm;
        
    }



}