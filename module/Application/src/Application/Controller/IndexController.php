<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Models\Tables\injectController;

class IndexController extends AbstractActionController
{
    
    private $tables = array();
    
    public function indexAction()
    {      
        $sm = $this->getServiceLocator();                
        $uri = $this->getRequest()->getRequestUri();         
        $ser = $this->getServiceName($uri); 
        
        try{      
            $dbTable= $sm->get($ser[0]); 
        }catch (\Exception $e){
            echo "service probléme !!!!";
        }
            $method = $ser[1];
        if($dbTable instanceof injectController){
        	$dbTable->setController($this);
        }else{
        	throw new \Exception('Table must implement injectController interface');
        }
        try{
            $liste = $dbTable->$method();
            return new JsonModel(array("results"=>$liste));
        }catch (\Exception $e){
        	//echo "methode probléme !!!!";
        }
        
    }
    
    
    
    public function getServiceName($uri){
    	$tbTables = $this->getServiceLocator()->get('config')['dbTable']; 
    	foreach ($tbTables as $k=>$v){
    		if(preg_match('#'.$k.'#', $uri)){
    			return $v ;
    		}
    	}
    }
    
   
}
