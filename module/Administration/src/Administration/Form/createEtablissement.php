<?php
namespace Administration\Form;


use Zend\Form\Form;
class createEtablissement extends Form{
	
    
	public function __construct($name = null, array $options = array()) {
		parent::__construct('createEtablissement');
		$this->add(array(
			'name'=>'name',
		    'type'=>'text',
		    'attributes'=>array(
			    'class'=>'form-control',
		        'id'=>'name_id'
		    )
		));

	}

}