

$(document).ready(function() {
	
	$('input[type="file"]').inputfile({
		
		uploadText: '<span class="glyphicon glyphicon-upload"></span> Select a file',	
		removeText: '<span class="glyphicon glyphicon-trash"></span>',		
		restoreText: '<span class="glyphicon glyphicon-remove"></span>',		
		uploadButtonClass: 'btn btn-primary',		
		removeButtonClass: 'btn btn-default',
		
		       initialPreview: [
		            'http://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/FullMoon2010.jpg/631px-FullMoon2010.jpg',
		            'http://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Earth_Eastern_Hemisphere.jpg/600px-Earth_Eastern_Hemisphere.jpg'
		        ],
		        initialPreviewAsData: true,
		        initialPreviewConfig: [
		            {caption: "Moon.jpg", size: 930321, width: "120px", key: 1, showDelete: false},
		            {caption: "Earth.jpg", size: 1218822, width: "120px", key: 2, showZoom: false}
		        ],
		        deleteUrl: "/site/file-delete",
		        overwriteInitial: true,
		        initialCaption: "The Moon and the Earth"
		    
		
		});

	
	
}); 