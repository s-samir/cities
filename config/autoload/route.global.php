<?php
return array(
        'router' => array(
                'routes' => array(
                        'etablissementadd' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                        'route'    => '/etablissement/add',
                                        'defaults' => array(
                                                __NAMESPACE__=>'Application',
                                                'controller' => 'Application\Controller\Index',
                                                'action'     => 'index',
                                        ),
                                ),
                        ),
                        'categorieadd' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                        'route'    => '/categorie/add',
                                        'defaults' => array(
                                                __NAMESPACE__=>'Application',
                                                'controller' => 'Application\Controller\Index',
                                                'action'     => 'index',
                                        ),
                                ),
                        ),
                        'addetablissement' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                        'route'    => '/admin/addetablissement',
                                        'defaults' => array(  
                                                __NAMESPACE__=>'Administration',                                              
                                                'controller' => 'Administration\Controller\Index',
                                                'action'     => 'index',
                                        ),
                                ),
                        ),
                        'addcategorie' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                        'route'    => '/admin/addcategorie',
                                        'defaults' => array(
                                                __NAMESPACE__=>'Administration',
                                                'controller' => 'Administration\Controller\Index',
                                                'action'     => 'addCategorie',
                                        ),
                                ),
                        ),
                        'listecategorie' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                        'route'    => '/listecategories',
                                        'defaults' => array(
                                                'controller' => 'Application\Controller\Index',
                                                'action'     => 'index',
                                        ),
                                ),
                        ),
                        'listcategoriechild' => array(
                                'type' => 'Zend\Mvc\Router\Http\Segment',
                                'options' => array(
                                        'route'    => '/listecategories/categorie_id/:d/parent/:d',
                                        'defaults' => array(
                                                'controller' => 'Application\Controller\Index',
                                                'action'     => 'index',
                                        ),
                                ),
                        ),
                        'etablissement' => array(
                                'type' => 'Zend\Mvc\Router\Http\Segment',
                                'options' => array(
                                        'route'    => '/ville/:d/categorie/:d/etablissement/:d',
                                        'defaults' => array(
                                                'controller' => 'Application\Controller\Index',
                                                'action'     => 'index',
                                        ),
                                ),
                        ),
                )
         )
);