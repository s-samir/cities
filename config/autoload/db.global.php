<?php
return array(
        'db' => array(
                'driver' => 'Pdo_Mysql',
                'database' => 'cities',
                'username' => 'cities',
                'password' => 'cities',
                'host'=>"195.110.35.225"
        ),
        'service_manager' => array(
                'factories' => array(
                        'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
                ),
            'invokables' => array(),
        ),
);